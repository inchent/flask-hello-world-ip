from flask import Flask, request
app = Flask(__name__)

@app.route('/')
def hello_world():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    print("IP:", ip_addr)
    return 'Hello, World!'
